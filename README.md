
<!-- README.md is generated from README.Rmd. Please edit that file -->

# toolboxfilecelia

<!-- badges: start -->
<!-- badges: end -->

The goal of toolboxfilecelia is to extract informations for files.

## Installation

You can install the development version of toolboxfilecelia like so:

``` r
# install.packages(toolboxfilecelia)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(toolboxfilecelia)
get_one_file_size(file = "analyse_facto.Rmd")
#>                name size
#> 1 analyse_facto.Rmd   NA
```
